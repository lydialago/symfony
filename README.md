# README #

Seguiu el procediment descrit, per instal·lar la demo de symfony en un entorn de producció. Primer ho heu de fer en una màquina local que controleu vosaltres i després al labs.iam.cat.

#APACHE
### Captura de pantalla de Symfony a Apache ###
![Symfony al navegador](/img/symfonyapache01.png)
![Symfony al navegador](/img/symfony_apache03.PNG)
### VirtualHost per a l'aplicació ###
![Arxiu de configuració del site](/img/symfonyapache02.png)

* [Pàgina web del manual seguit amb Apache](https://symfony.com/doc/3.4/setup/web_server_configuration.html)

Seguint el manual de Symfony es mou l'aplicació al directori /var/html. A l'Apache es crea l'arxiu de configuració del site al port 80 i s'especifica el documentroot al directori web del projecte. A l’arxiu de configuració d’Apache afegim o modifiquem la directica AllowOverride a All del directori /var/www per permetre el fitxer .htacces que hi ha al directori symfony-demo/web. El fixer .htacces es el que redirigeix amb un 302 cap a app.php
Es donen els permisos necessaris a tots els subdirectoris del projecte i s'activa el site.
El primer problema va ser referent a la ruta del documentroot però un cop solucionat el fitxer app.php ens donava error. Es va solucionar modificant els permisos dels subdirectoris i fitxers del projecte.

#LABS.IAM.CAT
* [Link a l'aplicació](http://labs.iam.cat/~a16lydlaglag/test1/web/)
### Captura de pantalla de Symfony al LABS ###
![Symfony al navegador](/img/symfony_labs01.PNG)
### Htaccess al directori arrel de l'aplicació i al directori web###
![htaccess](/img/symfony_labs02.PNG)

Per SCP hem passat tots els fitxers de l'aplicació al LABS. S'han donat permisos 777 a tots els fitxers perque amb 755 ens donava problemes per accedir-hi. S'ha creat un .htacces al directori arrel amb les directives "Required all granted" i "Options -Indexes" per poder accedir-hi però sense veure tota l'estructura de directoris. El fitxer .htacces de /web s'ha mantingut igual.

El principal problema que no he pogut resoldre es que un cop s'accedeix al directori de l'aplicació ens carregui directament el fitxer app.php del directori web.
